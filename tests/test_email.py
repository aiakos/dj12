from __future__ import absolute_import

from dj12._email import parse_email_url
import unittest


class EmailTestSuite(unittest.TestCase):
    def test_submit(self):
        url = 'submit://user@domain.com:password@smtp.example.com'
        conf = parse_email_url(url)
        self.assertEquals(conf['EMAIL_BACKEND'], 'django.core.mail.backends.smtp.EmailBackend')
        self.assertEquals(conf['EMAIL_HOST'], 'smtp.example.com')
        self.assertEquals(conf['EMAIL_PORT'], 587)
        self.assertEquals(conf['EMAIL_HOST_USER'], 'user@domain.com')
        self.assertEquals(conf['EMAIL_HOST_PASSWORD'], 'password')
        self.assertTrue(conf['EMAIL_USE_TLS'])
        self.assertFalse(conf['EMAIL_USE_SSL'])

    def test_console(self):
        url = 'console://'
        conf = parse_email_url(url)
        self.assertEquals(conf['EMAIL_BACKEND'], 'django.core.mail.backends.console.EmailBackend')
        self.assertIsNone(conf['EMAIL_HOST'])
        self.assertIsNone(conf['EMAIL_PORT'])
        self.assertIsNone(conf['EMAIL_HOST_USER'])
        self.assertIsNone(conf['EMAIL_HOST_PASSWORD'])
        self.assertFalse(conf['EMAIL_USE_TLS'])
        self.assertFalse(conf['EMAIL_USE_SSL'])

    def test_file(self):
        url = 'file:///tmp/app-messages'
        conf = parse_email_url(url)
        self.assertEquals(conf['EMAIL_BACKEND'], 'django.core.mail.backends.filebased.EmailBackend')
        self.assertEquals(conf['EMAIL_FILE_PATH'], "/tmp/app-messages")

    def test_submit_with_ssl(self):
        url = 'submit://user@domain.com:pass@smtp.example.com/?ssl=True'
        conf = parse_email_url(url)
        self.assertTrue(conf['EMAIL_USE_SSL'])
        self.assertFalse(conf['EMAIL_USE_TLS'])

    def test_submit_without_ssl(self):
        url = 'submit://user@domain.com:pass@smtp.example.com/?ssl=False'
        conf = parse_email_url(url)
        self.assertFalse(conf['EMAIL_USE_SSL'])
        self.assertTrue(conf['EMAIL_USE_TLS'])

    def test_submit_with_tls(self):
        url = 'submit://user@domain.com:pass@smtp.example.com/?tls=True'
        conf = parse_email_url(url)
        self.assertFalse(conf['EMAIL_USE_SSL'])
        self.assertTrue(conf['EMAIL_USE_TLS'])

    def test_submit_without_tls(self):
        url = 'submit://user@domain.com:pass@smtp.example.com/?tls=False'
        conf = parse_email_url(url)
        self.assertFalse(conf['EMAIL_USE_SSL'])
        self.assertFalse(conf['EMAIL_USE_TLS'])

    def test_legacy_smtp(self):
        url = '://user@domain.com:pass@smtp.example.com/'
        self.assertEquals(parse_email_url('smtp' + url), parse_email_url('submit' + url))

    def test_legacy_smtps(self):
        url = '://user@domain.com:pass@smtp.example.com/'
        self.assertEquals(parse_email_url('smtps' + url), parse_email_url('submit' + url))

    def test_special_chars(self):
        url = 'submit://user%21%40%23%245678:pass%25%5E%26%2A%28%29123@smtp.example.com'
        conf = parse_email_url(url)
        self.assertEquals(conf['EMAIL_HOST_PASSWORD'], 'pass%^&*()123')
        self.assertEquals(conf['EMAIL_HOST_USER'], 'user!@#$5678')
